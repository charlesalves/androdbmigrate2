package com.izanami.androdbmigrate2.database;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by charles.alves on 08/05/2015.
 */
public class SQLiteMigrationDatabase implements MigrationDatabase {

    private SQLiteDatabase database;

    public SQLiteMigrationDatabase(SQLiteDatabase database) {
        this.database = database;
    }

    @Override
    public void execSQL(String sql) {
        database.execSQL(sql);
    }

    @Override
    public void insert(String databaseVersionTable, String str, ContentValues contentValues) {
        database.insert(databaseVersionTable, str, contentValues);
    }

    @Override
    public void close() {
        database.close();
    }

    @Override
    public void beginTransaction() {
        database.beginTransaction();
    }

    @Override
    public void setTransactionSuccessful() {
        database.setTransactionSuccessful();
    }

    @Override
    public void endTransaction() {
        database.endTransaction();
    }
}
