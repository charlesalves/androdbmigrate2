package com.izanami.androdbmigrate2.migration;

import com.izanami.androdbmigrate2.database.MigrationDatabase;

/**
 * Created by Charles Alves on 29/04/2015.
 */
public interface DbMigration {

    void migrate(MigrationDatabase database);

}
