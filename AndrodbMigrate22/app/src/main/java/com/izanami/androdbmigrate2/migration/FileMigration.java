package com.izanami.androdbmigrate2.migration;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.izanami.androdbmigrate2.util.FileUtil;
import com.izanami.androdbmigrate2.database.MigrationDatabase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Charles Alves on 29/04/2015.
 */
public class FileMigration extends Migration {

    private String fileName;

    public FileMigration(String name) {
        super(FileUtil.RemoverExtencao(name));
        this.fileName = name;
    }

    @Override
    protected void onMigrate(Context context, MigrationDatabase database) {
        AssetManager assetManager = context.getAssets();
        try {
            database.beginTransaction();

            InputStream stream = assetManager.open("db/" + fileName);

            InputStreamReader reader = new InputStreamReader(stream);
            BufferedReader bufferedReader = new BufferedReader(reader);

            String linha = null;

            while ((linha = bufferedReader.readLine()) != null) {
                Log.v(getClass().getSimpleName(), linha);
                database.execSQL(linha);
            }

            database.setTransactionSuccessful();

        } catch (IOException e) {
            Log.e(getClass().getSimpleName(), "Problemas ao migrar o arquivo - " + name, e);
            throw new RuntimeException("O arquivo informado não foi encontrado - " + name, e);
        } finally {
            database.endTransaction();
        }

    }
}
