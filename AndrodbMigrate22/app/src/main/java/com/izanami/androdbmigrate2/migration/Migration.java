package com.izanami.androdbmigrate2.migration;

import android.content.ContentValues;
import android.content.Context;

import com.izanami.androdbmigrate2.helper.MigrationDbHelper;
import com.izanami.androdbmigrate2.database.MigrationDatabase;

/**
 * Created by Charles Alves on 29/04/2015.
 */
public abstract class Migration implements Comparable<Migration> {

    protected String name;

    protected Migration(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void migrate(Context context, MigrationDatabase database) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(MigrationDbHelper.COLLUMN_VERSION, this.name);

        database.insert(MigrationDbHelper.DB_VERSION_TABLE, null, contentValues);

        onMigrate(context, database);

    }

    @Override
    public int compareTo(Migration another) {
        return this.name.compareTo(another.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Migration migration = (Migration) o;

        if (!name.equals(migration.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    protected abstract void onMigrate(Context context, MigrationDatabase database);

}
