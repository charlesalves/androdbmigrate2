package com.izanami.androdbmigrate2.util;

import java.util.Arrays;

public class StringUtils {

	private StringUtils() {
	}

	public static boolean isEmpty(String string) {
		return string == null || "".equals(string.trim());
	}

	public static String[] toArray(String... entries) {
		return entries;
	}

	public static String concatStrings(Object... campos) {
		// Remove os cochetes do toString do Array
		// [1,2,3] = 1,2,3
		return Arrays.toString(campos).replaceAll("[\\[\\]]", "");
	}

}
