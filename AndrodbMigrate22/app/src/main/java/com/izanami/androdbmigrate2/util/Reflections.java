package com.izanami.androdbmigrate2.util;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import java.io.IOException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedList;

import dalvik.system.DexFile;
import dalvik.system.PathClassLoader;

public class Reflections {

    private String packageScan;
    private Context context;

    public Reflections(String packageScan, Context context) {
        this.packageScan = packageScan + '.';
        this.context = context;
    }

    @SuppressWarnings("unchecked")
    public <ClassType> Collection<Class<? extends ClassType>> getSubTypesOf(
            final Class<ClassType> clazz) {

        if (clazz == null) {
            throw new NullPointerException("Não é possível escanear uma classe nula");
        }

        if (!clazz.isInterface()) {
            throw new IllegalArgumentException("Clazz deve ser uma interface");
        }

        Collection<Class<? extends ClassType>> classes = scanPackageClasses();

        return CollectionsUtil.filter(classes, new CollectionsUtil.Filter<Class<? extends ClassType>>() {
            @Override
            public boolean filter(Class<? extends ClassType> t1) {
                return clazz.isAssignableFrom(t1);
            }
        });
    }

    public <ClassType> Collection<Class<? extends ClassType>> scanPackageClasses() {

        Collection<Class<? extends ClassType>> classes = new LinkedList<>();
        try {
            String apk = context.getPackageManager().getApplicationInfo(
                    context.getPackageName(), 0).sourceDir;
            DexFile dexFile = new DexFile(apk);

            PathClassLoader classLoader = new PathClassLoader(apk, Thread
                    .currentThread().getContextClassLoader());

            Enumeration<String> entries = dexFile.entries();

            while (entries.hasMoreElements()) {
                String string = (String) entries.nextElement();

                if (string.startsWith(packageScan)) {

                    Log.v(getClass().getSimpleName(), string);

                    Class<?> entryClass = classLoader.loadClass(string);
                    if (entryClass != null && !entryClass.isInterface()) {
                        classes.add((Class<ClassType>) entryClass);
                    }
                }
            }

        } catch (NameNotFoundException | IOException | ClassNotFoundException e) {
            Log.e(getClass().getSimpleName(), e.getMessage(), e);
        }

        return classes;
    }

}
