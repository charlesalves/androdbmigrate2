package com.izanami.androdbmigrate2.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Charles Alves on 29/04/2015.
 */
public class CollectionsUtil {

    public static <T> Collection<T> filter(Collection<T> collection, Filter<T> filter) {
        List<T> list = new ArrayList<>(collection.size());
        for (T t : collection) {
            if (filter.filter(t)) {
                list.add(t);
            }
        }
        return list;
    }

    public static interface Filter<T> {

        boolean filter(T t1);

    }

}
