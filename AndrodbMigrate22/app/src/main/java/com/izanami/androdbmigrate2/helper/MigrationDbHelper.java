package com.izanami.androdbmigrate2.helper;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.izanami.androdbmigrate2.database.MigrationDatabase;
import com.izanami.androdbmigrate2.database.SQLiteMigrationDatabase;
import com.izanami.androdbmigrate2.migration.ClassMigration;
import com.izanami.androdbmigrate2.migration.DbMigration;
import com.izanami.androdbmigrate2.migration.FileMigration;
import com.izanami.androdbmigrate2.migration.Migration;
import com.izanami.androdbmigrate2.util.FileUtil;
import com.izanami.androdbmigrate2.util.Reflections;
import com.izanami.androdbmigrate2.util.StringUtils;
import com.izanami.androdbmigrate2.util.TableBuilder;

import java.io.IOException;
import java.util.Collection;
import java.util.TreeSet;

/**
 * Created by Charles Alves on 29/04/2015.
 */
public class MigrationDbHelper extends SQLiteOpenHelper {

    public static final String COLLUMN_VERSION = "VERSION";
    public static final String DB_VERSION_TABLE = "DB_VERSION";

    private Context context;
    private String packageScan;

    public MigrationDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, String packageScan) {
        super(context, name, factory, version);
        this.context = context;
        this.packageScan = packageScan;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        TableBuilder builder = new TableBuilder(DB_VERSION_TABLE)
                .setPrimaryKey("_id", TableBuilder.INTEGER, true)
                .addColuna(COLLUMN_VERSION, TableBuilder.TEXT, true);

        db.execSQL(builder.toString());

        migrate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        migrate(db);
    }

    private void migrate(SQLiteDatabase database) {

        String version = getDatabaseVersion(database);

        Collection<Migration> migrations = new TreeSet<>();

        Collection<Migration> classMigrations = getClassMigrations(version);
        Collection<Migration> fileMigrations = getFileMigrations(version);

        migrations.addAll(classMigrations);
        migrations.addAll(fileMigrations);

        MigrationDatabase migrationDatabase = new SQLiteMigrationDatabase(database);

        for (Migration migration : migrations) {
            Log.d(getClass().getSimpleName(), "Realizando migração - "
                    + migration.getName());
            migration.migrate(this.context, migrationDatabase);
        }

        Log.i(getClass().getSimpleName(), "Finalizando Migração");
    }

    private Collection<Migration> getFileMigrations(String version) {
        Collection<Migration> migrations = new TreeSet<>();

        // Estou deixando o processamento sendo realizado em dois metodos para evitar que uma
        // possivel ecessão lançada no processamento do arquivo atrapalhe o processamento das
        // classes
        AssetManager assetManager = context.getAssets();

        try {
            String[] files = assetManager.list("db");

            if (version == null) {
                for (String file : files) {
                    migrations.add(new FileMigration(file));
                }
            } else {
                for (String file : files) {
                    if (version.compareTo(FileUtil.RemoverExtencao(file)) > 0) {
                        migrations.add(new FileMigration(file));
                    }
                }
            }

        } catch (IOException e) {
            throw new RuntimeException("Não foi possível encontrar o arquivo informado");
        }
        return migrations;
    }

    private Collection<Migration> getClassMigrations(String version) {
        Reflections reflections = new Reflections(packageScan, context);

        Collection<Class<? extends DbMigration>> migrationsClass =
                reflections.getSubTypesOf(DbMigration.class);

        Collection<Migration> migrations = new TreeSet<>();

        if (version == null) {
            Log.i(getClass().getSimpleName(), "Iniciando Migracao - " + COLLUMN_VERSION);
            for (Class<? extends DbMigration> migrationClass : migrationsClass) {
                migrations.add(new ClassMigration(migrationClass));
            }
        } else {
            Log.i(getClass().getSimpleName(), "Iniciando Migracao de criacao ");
            for (Class<? extends DbMigration> migrationClass : migrationsClass) {
                if (migrationClass.getSimpleName().compareTo(version) > 0) {
                    migrations.add(new ClassMigration(migrationClass));
                }
            }
        }
        return migrations;
    }

    private String getDatabaseVersion(SQLiteDatabase db) {
        Cursor cursor = db.query(DB_VERSION_TABLE,
                StringUtils.toArray(COLLUMN_VERSION), null, null, null, null,
                "_id DESC", "1");

        String versao = null;

        if (cursor.moveToFirst()) {
            versao = cursor.getString(0);
        }

		cursor.close();
        return versao;
    }

}
