package com.izanami.androdbmigrate2.util;

import java.util.HashMap;

import android.util.Log;

/**
 * 
 * @author Rodrigo Brasileiro
 * 
 */
public class TableBuilder {

	public static enum Action {
		NO_ACTION("NO ACTION"), RESTRICT("RESTRICT"), SET_NULL("SET NULL"), SET_DEFAULT(
				"SET DEFAULT"), CASCADE("CASCADE");

		String action;

		private Action(String action) {
			this.action = action;
		}

		@Override
		public String toString() {
			return action;
		}
	}

	private static boolean PRINT_CREATION = true;

	private static final String VIRGULA = ",";

	public static final String INTEGER = "INTEGER";
	public static final String REAL = "REAL";
	public static final String TEXT = "TEXT";
	public static final String DATE = "DATE";
	public static final String DATE_TIME = "DATETIME ";

	private String tabela;
	private HashMap<String, String> pks;
	private HashMap<String, String> colunas;
	private HashMap<String, String> fks;

	public TableBuilder(String tabela) {
		this.tabela = tabela;
		colunas = new HashMap<String, String>();
		pks = new HashMap<String, String>();
		fks = new HashMap<String, String>();
	}

	public TableBuilder setPrimaryKey(String coluna, String tipo,
			boolean autoInc) {
		if (autoInc) {
			coluna = coluna + " AUTOINCREMENT ";
		}
		setPrimaryKey(new String[] { coluna }, new String[] { tipo });

		return this;
	}

	public TableBuilder setPrimaryKey(String[] colunas, String[] tipos) {
		int parada = colunas.length < tipos.length ? colunas.length
				: tipos.length;
		for (int i = 0; i < parada; ++i) {

			if (!pks.containsKey(colunas[i])) {
				pks.put(colunas[i], tipos[i]);
			} else {
				throw new RuntimeException("Colunas repetidas!: " + colunas[i]);
			}

			// UMA PK pode ser FK
			this.addColuna(colunas[i].replace(" AUTOINCREMENT ", ""), tipos[i],
					false, false);

		}

		return this;
	}

	private TableBuilder addColuna(String nome, String tipo, boolean notNull,
			boolean checkColunas) {
		if (!colunas.containsKey(nome)) {
			colunas.put(nome, nome + " " + tipo
					+ (notNull ? " NOT NULL " + VIRGULA : VIRGULA));
		} else if (checkColunas) {
			throw new RuntimeException("Colunas repetidas!: " + nome);
		}

		return this;
	}

	public TableBuilder addColuna(String nome, String tipo, boolean notNull) {
		this.addColuna(nome, tipo, notNull, true);

		return this;
	}

	public TableBuilder addFK(String nome, String tipo, String tabelaRef,
			String colunaRef, Action actionDelete, Action actionUpdate) {

		// Uma FK pode ser PK
		this.addColuna(nome, tipo, false, false);

		fks.put(nome, "CONSTRAINT FK_" + nome + " FOREIGN KEY (" + nome
				+ ")  REFERENCES " + tabelaRef + " (" + colunaRef + ") "
				+ "ON DELETE " + actionDelete.toString() + " ON UPDATE "
				+ actionUpdate.toString() + VIRGULA);

		return this;

	}

	public TableBuilder addFK_PKMultiple(String[] nomes, String[] tipos,
			String tabelaRef, Action actionDelete, Action actionUpdate) {
		int parada = nomes.length < tipos.length ? nomes.length : tipos.length;
		String constraintNome = "";
		String nomesColunas = "";

		for (int i = parada - 1; i > -1; --i) {
			if (i == parada - 1) {
				constraintNome = nomes[i] + "__";
				nomesColunas = nomes[i];
			} else {
				constraintNome = nomes[i] + "_" + constraintNome;
				nomesColunas = nomes[i] + VIRGULA + nomesColunas;

			}
			this.addColuna(nomes[i], tipos[i], false, false);
		}
		fks.put(nomesColunas, "CONSTRAINT FK_" + constraintNome
				+ " FOREIGN KEY (" + nomesColunas + ")  REFERENCES "
				+ tabelaRef + " ON DELETE " + actionDelete.toString()
				+ " ON UPDATE " + actionUpdate.toString() + VIRGULA);

		return this;

	}

	public TableBuilder addFK(String[] nomes, String[] tipos, String tabelaRef,
			String[] colunasRef, Action actionDelete, Action actionUpdate) {
		int parada = nomes.length < tipos.length ? nomes.length : tipos.length;
		String constraintNome = "";
		String nomesColunas = "";
		String nomesColunasRef = "";

		for (int i = 0; i < parada; ++i) {
			if (i == 0) {
				constraintNome = nomes[i] + "_";
				nomesColunas = nomes[i];
				nomesColunasRef = colunasRef[i];
			} else {
				constraintNome = nomes[i] + "_" + constraintNome;
				nomesColunas = nomes[i] + VIRGULA + nomesColunas;
				nomesColunasRef = colunasRef[i] + VIRGULA + nomesColunasRef;

			}
			this.addColuna(nomes[i], tipos[i], false, false);
		}
		fks.put(nomesColunas,
				"CONSTRAINT FK_" + constraintNome + " FOREIGN KEY ("
						+ nomesColunas + ")  REFERENCES " + tabelaRef + " ("
						+ nomesColunasRef + ") " + "ON DELETE "
						+ actionDelete.toString() + " ON UPDATE "
						+ actionUpdate.toString() + VIRGULA);

		return this;

	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("CREATE TABLE ");
		sb.append(tabela);
		sb.append(" (");

		for (String col : colunas.values()) {
			sb.append(col);
		}

		if (pks != null && !pks.isEmpty()) {
			sb.append(" PRIMARY KEY (");

			int sizePk = pks.size();
			int i = 0;
				
			for (String pk : pks.keySet()) {
				if (i < sizePk - 1) {
					sb.append(pk + VIRGULA);
				} else {
					sb.append(pk);
				}
				i = i + 1;
			}

			sb.append(") ");
		}

		for (String fk : fks.values()) {
			sb.append(fk);
		}

		String teste = sb.toString();

		if (teste.endsWith(VIRGULA)) {
			String t = teste.substring(0, teste.length() - VIRGULA.length())
					+ "); ";
			if (PRINT_CREATION) {
				Log.v(getClass().getSimpleName(), t);
			}
			return t;

		} else {

			sb.append("); ");
			if (PRINT_CREATION) {
				Log.v(getClass().getSimpleName(), sb.toString());
			}
			return sb.toString();
		}

	}

}
