package com.izanami.androdbmigrate2.migration;

import android.content.Context;
import android.util.Log;

import com.izanami.androdbmigrate2.database.MigrationDatabase;

/**
 * Created by Charles Alves on 29/04/2015.
 */
public class ClassMigration extends Migration {

    private Class<? extends DbMigration> clazz;

    public ClassMigration(Class<? extends DbMigration> clazz) {
        super(clazz.getSimpleName());
        this.clazz = clazz;
    }

    @Override
    protected void onMigrate(Context context, MigrationDatabase database) {
        try {
            database.beginTransaction();
            clazz.newInstance().migrate(database);
            database.setTransactionSuccessful();
        } catch (InstantiationException | IllegalAccessException e) {
            String message = "Erro ao tentar migrar a classe - " + getName();
            Log.e(getClass().getSimpleName(), message, e);
            throw new RuntimeException(message, e);
        } finally {
            database.endTransaction();
        }
    }
}
