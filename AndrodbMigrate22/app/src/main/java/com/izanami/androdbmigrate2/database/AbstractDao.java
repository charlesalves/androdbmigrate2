package com.izanami.androdbmigrate2.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.izanami.androdbmigrate2.helper.MigrationDbHelper;
import com.izanami.androdbmigrate2.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Charles Alves on 09/05/2015.
 */
public abstract class AbstractDao<Tipo extends AbstractDao.Insertable> {

    private MigrationDbHelper databaseAdapter;
    private SQLiteDatabase database;

    public AbstractDao(MigrationDbHelper databaseAdapter) {
        this.databaseAdapter = databaseAdapter;
        this.database = databaseAdapter.getWritableDatabase();
    }

    public void close() {
        database.close();
        databaseAdapter.close();
    }

    /**
     * Adiciona ou atualiza um inserteble ao banco
     *
     * @param insertable Objeto a ser persistido no banco
     * @return retorna o id do Objeto adicionado ao banco
     */
    public long save(Tipo insertable) {

        ContentValues contentValues = getContentValues(insertable);

        if (insertable.getId() == 0) {
            long id = getDatabase().insert(getTabela(), null, contentValues);
            insertable.setId(id);
        } else {
            getDatabase().update(getTabela(), contentValues,
                    getWhereIdQuery(), getStringArrayId(insertable.getId()));
        }

        return insertable.getId();
    }

    /**
     * Adiciona ou atualiza vários insertables ao mesmo tempo no banco de dados
     *
     * @param insertables Lista de Objetos que seram inseridos no banco
     */
    public void save(Collection<Tipo> insertables) {

        if (insertables == null) {
            throw new RuntimeException("Não é possível salvar uma lista nula");
        }

        if (insertables.isEmpty()) {
            return;
        }

        try {
            getDatabase().beginTransaction();
            for (Tipo insertable : insertables) {
                save(insertable);
            }
            getDatabase().setTransactionSuccessful();
        } finally {
            getDatabase().endTransaction();
        }

    }

    /**
     * Busca um Objeto a partir do seu id
     *
     * @param id id do objeto que se desera buscar no banco
     * @return retorna o objeto caso seja encontrado e null caso ele não esteja no banco
     */
    public Tipo find(long id) {
        return findBy(getWhereIdQuery(),
                getStringArrayId(id), null, null);
    }

    /**
     * Lista todos os objetos persistidos no banco de dados
     *
     * @return Lista com todos os objetos do banco de dados
     */
    public List<Tipo> list() {
        return listBy(null, null, null, null);
    }

    public void delete(long id) {
        if (id == 0) {
            return;
        }
        database.delete(getTabela(), getWhereIdQuery(), getStringArrayId(id));
    }

    public void delete(Tipo insertable) {
        delete(insertable.getId());
    }

    public MigrationDbHelper getDatabaseAdapter() {
        return databaseAdapter;
    }

    protected SQLiteDatabase getDatabase() {
        return database;
    }

    /**
     * Cria uma lista com todos os elementos contidos no cursor
     *
     * @param cursor Cursor que se deseja obter os Objetos
     * @return Lista com instâncias do Objetos criados a partir do cursor
     */
    protected List<Tipo> getResultsByCursor(Cursor cursor) {
        List<Tipo> result = new ArrayList<Tipo>();

        while (cursor.moveToNext()) {
            result.add(getInstanceByCursor(cursor));
        }

        cursor.close();

        return result;
    }

    /**
     * Retorna somente o primeiro Objeto do cursor
     *
     * @param cursor Cursor contendo um unico resultado
     * @return Instância do Objeto criado a partir da primeira posição do cursor
     */
    protected Tipo getUniqueResultByCursor(Cursor cursor) {
        Tipo retorno = null;
        if (cursor.moveToFirst()) {
            retorno = getInstanceByCursor(cursor);
        }

        cursor.close();

        return retorno;
    }

    protected Tipo findBy(String section, String[] sectionArgs,
                          String groupBy, String orderBy) {
        Cursor cursor = createCursor(section, sectionArgs, groupBy, orderBy);

        return getUniqueResultByCursor(cursor);
    }

    protected List<Tipo> listBy(String section, String[] sectionArgs,
                                String groupBy, String orderBy) {
        Cursor cursor = createCursor(section, sectionArgs, groupBy, orderBy);

        return getResultsByCursor(cursor);
    }

    protected abstract ContentValues getContentValues(Tipo insertable);

    protected abstract String getTabela();

    protected abstract String getColunaId();

    protected abstract String[] getColunas();

    protected abstract Tipo getInstanceByCursor(Cursor cursor);

    private Cursor createCursor(String section, String[] sectionArgs,
                                String groupBy, String orderBy) {
        return getDatabase().query(getTabela(), getColunas(), section,
                sectionArgs, groupBy, null, orderBy);
    }

    private String getWhereIdQuery() {
        return getColunaId() + " = ?";
    }

    private String[] getStringArrayId(long id) {
        return StringUtils.toArray(String.valueOf(id));
    }

    public interface Insertable {

        long getId();

        void setId(long id);

    }
}
