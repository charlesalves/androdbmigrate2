package com.izanami.androdbmigrate2.database;

import android.content.ContentValues;

/**
 * Created by charles.alves on 07/05/2015.
 */
public interface MigrationDatabase {

    void execSQL(String sql);

    void insert(String databaseVersionTable, String str, ContentValues contentValues);

    void close();

    void beginTransaction();

    void setTransactionSuccessful();

    void endTransaction();
}
