package com.izanami.androdbmigrate2.integration.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;
import android.test.suitebuilder.annotation.MediumTest;

import com.izanami.androdbmigrate2.helper.MigrationDbHelper;
import com.izanami.androdbmigrate2.util.TableBuilder;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

/**
 * Created by charles.alves on 08/05/2015.
 */
public class MigrationDbHelperTest extends AndroidTestCase {

    private Context context;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        context = new RenamingDelegatingContext(getContext(), "test_");
        System.setProperty("dexmaker.dexcache", context.getCacheDir().getPath());
    }

    @MediumTest
    public void testCreateDatabase() {
        MigrationDbHelper dbHelper = createMigrationDbHelper(1);
        SQLiteDatabase database = dbHelper.getReadableDatabase();

        verify(dbHelper).onCreate(any(SQLiteDatabase.class));
        testTables(database);

        database.close();
        dbHelper.close();

    }

    private MigrationDbHelper createMigrationDbHelper(int version) {
        return spy(new MigrationDbHelper(context, "ANDROID_MIGRATION", null,
                version, "com.izanami.androdbmigrate2.integration.migration"));
    }

    @MediumTest
    public void testUpdateDatabase() {
        iniciarDatabase();

        MigrationDbHelper dbHelper = createMigrationDbHelper(2);
        SQLiteDatabase database = dbHelper.getReadableDatabase();

        verify(dbHelper).onUpgrade(any(SQLiteDatabase.class), eq(1), eq(2));
        testTables(database);

        database.close();
        dbHelper.close();
    }

    private void iniciarDatabase() {
        SQLiteOpenHelper openHelper = new SQLiteOpenHelper(context, "ANDROID_MIGRATION", null, 1) {
            @Override
            public void onCreate(SQLiteDatabase db) {


                TableBuilder builder = new TableBuilder("DB_VERSION")
                        .setPrimaryKey("_id", TableBuilder.INTEGER, true)
                        .addColuna("VERSION", TableBuilder.TEXT, true);

                db.execSQL(builder.toString());
            }

            @Override
            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            }
        };
        SQLiteDatabase database = openHelper.getReadableDatabase();
        database.close();
        openHelper.close();

    }

    private void testTables(SQLiteDatabase database) {
        Cursor cursor = database.rawQuery("select DISTINCT tbl_name from sqlite_master", null);

        assertThat(cursor.getCount(), equalTo(6));
        List<String> tabelas = new ArrayList<>(6);
        while (cursor.moveToNext()) {
            tabelas.add(cursor.getString(0));
        }
        assertThat(tabelas, hasItems("VERSION", "TEST", "ANDROID", "DB_VERSION"));
        cursor.close();

        cursor = database.query("DB_VERSION", new String[]{"VERSION"}, null, null, null, null, null);

        assertThat(cursor.getCount(), equalTo(2));
        List<String> verions = new ArrayList<>(2);

        while (cursor.moveToNext()) {
            verions.add(cursor.getString(0));
        }

        assertThat(verions, contains("V20150505-01", "V20150508_1_createTableTest"));
    }

}
