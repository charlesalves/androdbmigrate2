package com.izanami.androdbmigrate2.unit.migration;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;
import android.test.suitebuilder.annotation.SmallTest;

import com.izanami.androdbmigrate2.migration.FileMigration;
import com.izanami.androdbmigrate2.database.MigrationDatabase;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by charles.alves on 06/05/2015.
 */
public class FileMigrationTest extends AndroidTestCase {

    private Context context;
    private SQLiteOpenHelper openHelper;
    private MigrationDatabase database;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        context = new RenamingDelegatingContext(getContext(), "test_");
        database = mock(MigrationDatabase.class);
    }

    @SmallTest
    public void testMigrationFileNotFound() {
        String fileName = "V20150505-01.sq";
        try {
            new FileMigration(fileName).migrate(context, database);
            fail();
        } catch (RuntimeException e) {
            assertThat(e.getCause(), instanceOf(IOException.class));
        }
    }

    @SmallTest
    public void testMigrationFileFound() {
        new FileMigration("V20150505-01.sql").migrate(context, database);

        verify(database, times(2)).execSQL(anyString());

        verify(database).execSQL("CREATE TABLE ANDROID (_id INTEGER PRIMARY KEY, VERSION TEXT, VERSION_CODE INTEGER, CREATED DATETIME);");
        verify(database).execSQL("CREATE TABLE VERSION (_id INTEGER PRIMARY KEY, VERSION TEXT);");

    }

}
