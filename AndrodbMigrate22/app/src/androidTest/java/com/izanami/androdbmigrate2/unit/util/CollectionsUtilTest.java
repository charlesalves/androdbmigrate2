package com.izanami.androdbmigrate2.unit.util;

import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.SmallTest;

import com.izanami.androdbmigrate2.util.CollectionsUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

/**
 * Created by Charles Alves on 29/04/2015.
 */
public class CollectionsUtilTest extends AndroidTestCase {

    @SmallTest
    public void testFilter() {
        List<String> compare = Arrays.asList("a", "b", "c", "d", "e");

        final List<String> filter = Arrays.asList("b", "d", "e");

        Collection<String> collection = CollectionsUtil.filter(compare, new CollectionsUtil.Filter<String>() {
            @Override
            public boolean filter(String t1) {
                return filter.contains(t1);
            }
        });

        assertThat("A quantidade de elementos filtrados não corresponde ao esperado",
                collection, hasSize(3));

        assertThat("A colessao retornada não corresponde aos elementos esperados",
                new ArrayList<String>(collection), equalTo(filter));

    }

}
