package com.izanami.androdbmigrate2.integration.migration;

import com.izanami.androdbmigrate2.database.MigrationDatabase;
import com.izanami.androdbmigrate2.migration.DbMigration;
import com.izanami.androdbmigrate2.util.TableBuilder;

/**
 * Created by charles.alves on 08/05/2015.
 */
public class V20150508_1_createTableTest implements DbMigration {
    @Override
    public void migrate(MigrationDatabase database) {
        TableBuilder tableBuilder = null;
        tableBuilder = new TableBuilder("TEST")
                .setPrimaryKey("_id", TableBuilder.INTEGER, true)
                .addColuna("testando", TableBuilder.TEXT, false);
        database.execSQL(tableBuilder.toString());
    }
}
