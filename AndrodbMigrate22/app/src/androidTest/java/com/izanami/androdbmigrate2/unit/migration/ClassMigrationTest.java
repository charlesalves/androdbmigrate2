package com.izanami.androdbmigrate2.unit.migration;

import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.SmallTest;

import com.izanami.androdbmigrate2.migration.ClassMigration;
import com.izanami.androdbmigrate2.unit.migration.classToScam.AbstractClassMigration;
import com.izanami.androdbmigrate2.unit.migration.classToScam.ClassWithPrivateConstrutorMigration;
import com.izanami.androdbmigrate2.unit.migration.classToScam.ClassWithoutEmptyConstrutorMigration;
import com.izanami.androdbmigrate2.database.MigrationDatabase;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.mockito.Mockito.mock;

/**
 * Created by charles.alves on 08/05/2015.
 */
public class ClassMigrationTest extends AndroidTestCase {

    private MigrationDatabase database;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        database = mock(MigrationDatabase.class);

    }

    @SmallTest
    public void testMigrationWithAbstractClass() {
        try {
            new ClassMigration(AbstractClassMigration.class)
                    .migrate(getContext(), database);
            fail("A classe não lançou a exessão por estar tentando instanciar uma classe abstrata");
        } catch (RuntimeException e) {
            assertThat(e.getCause(), instanceOf(InstantiationException.class));
        }
    }

    @SmallTest
    public void testMigrationWithClassWithoutEmptyConstrutor() {
        try {
            new ClassMigration(ClassWithoutEmptyConstrutorMigration.class)
                    .migrate(getContext(), database);
            fail("A classe não lançou a exessão como esperado");
        } catch (RuntimeException e) {
            assertThat(e.getCause(), instanceOf(InstantiationException.class));
        }
    }

    @SmallTest
    public void testMigrationWithPrivateConstrutor() {
        try {
            new ClassMigration(ClassWithPrivateConstrutorMigration.class)
                    .migrate(getContext(), database);
            fail();
        } catch (Exception e) {
            assertThat(e.getCause(), instanceOf(IllegalAccessException.class));
        }
    }

}
