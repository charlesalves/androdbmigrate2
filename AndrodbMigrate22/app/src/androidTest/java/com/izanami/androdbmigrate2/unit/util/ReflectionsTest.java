package com.izanami.androdbmigrate2.unit.util;

import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.SmallTest;

import com.izanami.androdbmigrate2.unit.model.Class1TestScannable;
import com.izanami.androdbmigrate2.unit.model.subpackage.Class2TestScannable;
import com.izanami.androdbmigrate2.unit.model.Class3TestDontScannable;
import com.izanami.androdbmigrate2.unit.model.Scannable;
import com.izanami.androdbmigrate2.util.Reflections;

import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;

/**
 * Created by Charles Alves on 05/05/2015.
 */
public class ReflectionsTest extends AndroidTestCase {

    private Reflections reflections;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        reflections = new Reflections("com.izanami.androdbmigrate2.unit.model", getContext());
    }

    @SmallTest
    public void testGetSubTypesOfWithInterfaceParameter() {
        Collection<Class<? extends Scannable>> classes = reflections.getSubTypesOf(Scannable.class);

        assertThat(classes, hasSize(2));
        assertThat(classes, hasItems(Class1TestScannable.class, Class2TestScannable.class));

    }

    @SmallTest
    public void testGetSubTypesOfWithClassParameter() {
        try {
            Collection<Class<? extends Class1TestScannable>> classes = reflections.getSubTypesOf(Class1TestScannable.class);
            fail("Não foi lançada a exception por tentar escanear uma classe");
        } catch (Exception e) {
        }
    }

    @SmallTest
    public void testScanPackage() {
        Collection<Class<?>> classes = reflections.scanPackageClasses();

        assertThat(classes, hasSize(3));
        assertThat(classes, hasItems(Class1TestScannable.class, Class2TestScannable.class,
                Class3TestDontScannable.class));
    }

}
