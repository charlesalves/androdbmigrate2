package com.izanami.androdbmigrate2.unit.migration.classToScam;

import com.izanami.androdbmigrate2.migration.DbMigration;
import com.izanami.androdbmigrate2.database.MigrationDatabase;

/**
 * Created by charles.alves on 08/05/2015.
 */
public abstract class AbstractClassMigration implements DbMigration {

    @Override
    public void migrate(MigrationDatabase database) {

    }
}
